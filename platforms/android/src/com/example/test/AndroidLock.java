package com.example.test;

import android.app.Activity;
import android.content.Intent;

public class AndroidLock extends Activity {

	void setAttributes(int paramInt1, boolean paramBoolean, int paramInt2){
		Intent localIntent = new Intent(this, LayoutService.class);
		localIntent.putExtra("orientation", paramInt1);
		localIntent.putExtra("persistent", paramBoolean);
		localIntent.putExtra("selIndex", paramInt2);
		startService(localIntent);
	}
}
